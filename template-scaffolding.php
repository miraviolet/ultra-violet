<?php
/**
 * Template Name: Scaffolding
 *
 * Template Post Type: page, scaffolding, uv_scaffolding
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Ultra Violet
 */

get_header(); ?>

	<div class="primary content-area">
		<main id="main" class="site-main">
			<?php do_action( 'uv_scaffolding_content' ); ?>
		</main><!-- #main -->
	</div><!-- .primary -->

<?php get_footer(); ?>
