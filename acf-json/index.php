<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Ultra Violet
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
