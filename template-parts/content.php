<?php
/**
 * Template part for displaying posts in an archive feed, or blog feed.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Ultra Violet
 */

?>

<article class="blog-post"<?php post_class();?>>
	<div class="entry">
		<?php if (has_post_thumbnail()) { ?>
			<figure class="featured-image index-image">
				<a href="<?php echo esc_url(get_permalink()) ?>" rel="bookmark" target="_blank">
					<?php
						the_post_thumbnail('blog_thumbnail');
					?>
				</a>
			</figure><!-- .featured-image full-bleed -->
		<?php } ?>

		<div class="entry-meta">
			<span class="category"><?php echo get_the_category()[0]->name; ?></span>
		</div><!-- .entry-meta -->

		<header class="entry-header">
			<?php
				if (is_single()):

					the_title('<h1 class="entry-title">', '</h1>');

				else:

					the_title('<h2 class="entry-title"><a href="' . esc_url(get_permalink()) . '" rel="bookmark" target="_blank">', '</a></h2>');

				endif;

				if ('post' === get_post_type()):

				endif;
			?>
		</header><!-- .entry-header -->

		<div class="entry-content">
			<div class="exerpt"><?php the_excerpt($post->post_content, get_the_excerpt()); ?></div>

			<p class="author">
				<span class="author-pic"><?php echo get_avatar(get_the_author_meta('ID'), 32); ?></span>

				<span class="author-text">by, </span>

				<span class="author-name"> 
					<?php echo get_the_author_meta('user_nicename', $post->post_author); ?>
				</span>
			</p>
			
			<?php

			wp_link_pages(array(
				'before' => '<div class="page-links">' . esc_html__('Pages:', 'protech'),
				'after' => '</div>',
			));

			?>
		</div><!-- .entry-content -->
	</div><!-- .entry -->
</article><!-- #post-## -->

