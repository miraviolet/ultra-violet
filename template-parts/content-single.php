<?php
/**
 * Template part for displaying single posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Ultra Violet
 */
// $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
?>
<!-- ARTICLE CONTENT START -->
<article class="blog-post"<?php post_class();?>>
	<div class="entry">
		<!-- ENTRY HEADER START -->
		<header class="entry-header" style="background-image: url(' <?php the_field('hero_image'); ?> '); background-size: cover;">
			<div class="hero-overlay">
				<?php
					if (is_single()):
						the_title('<h1 class="entry-title">', '</h1>');
					else:
						the_title('<h2 class="entry-title"><a href="' . esc_url(get_permalink()) . '" rel="bookmark">', '</a></h2>');
					endif;
					if ('post' === get_post_type()):
					?>
				<?php endif;?>
				<!-- ENTRY META START -->
				<div class="entry-meta">
						<?php echo get_the_category()[0]->name; ?>
				</div><!-- .entry-meta -->
				<!-- ENTRY META END -->
				<p class="author"><?php echo get_avatar(get_the_author_meta('ID'), 32); ?><span class="author-text">by, </span><span class="author-name"> <?php echo get_the_author_meta('user_nicename', $post->post_author); ?></span></p>
			</div>
		</header><!-- .entry-header -->
		<!-- ENTRY HEADER END -->
		<!-- ENTRY CONTENT START -->
		<div class="entry-content">
			<?php
				the_content();
				
				// wp_link_pages(array(
				// 	'before' => '<div class="page-links">' . esc_html__('Pages:', 'ultra-violet'),
				// 	'after' => '</div>',
				// ));
			?>
		</div><!-- .entry-content -->
		<!-- ENTRY CONTENT END -->
	</div><!-- .entry -->
</article><!-- #post-## -->
<!-- ARTICLE CONTENT END -->